# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the neutron-dynamic-routing package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: neutron-dynamic-routing\n"
"Report-Msgid-Bugs-To: neutron-dynamic-routing@packages.debian.org\n"
"POT-Creation-Date: 2019-03-14 16:24+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../neutron-dynamic-routing-common.templates:1001
msgid "Run default configuration for neutron-dynamic-routing ?"
msgstr ""

#. Type: boolean
#. Description
#: ../neutron-dynamic-routing-common.templates:1001
msgid ""
"If you want to run now, please make sure you have configured database."
"connection in neutron.conf. Database migrations will be executed."
msgstr ""

#. Type: boolean
#. Description
#: ../neutron-dynamic-routing-common.templates:1001
msgid ""
"If you don't choose this option, no database migration will be run and no "
"plugin will be enabled, these things you have to do manually."
msgstr ""

#. Type: boolean
#. Description
#: ../neutron-dynamic-routing-common.templates:1001
msgid ""
"You can change this setting later on by running \"dpkg-reconfigure -plow "
"python3-neutron-fwaas\"."
msgstr ""

#. Type: string
#. Description
#: ../neutron-dynamic-routing-common.templates:2001
msgid "Neutron-dynamic-routing router-id:"
msgstr ""

#. Type: string
#. Description
#: ../neutron-dynamic-routing-common.templates:2001
msgid ""
"Neutron-dynamic-routing needs to be able to communicate with remote BGP "
"system. Therefore Neutron-dynamic-routing needs to know local BGP router id. "
"Please fill router id with a suitable unique 32-bit number, typically an "
"IPv4 address on the host running the agent. For example, 192.168.0.2."
msgstr ""

#. Type: string
#. Description
#: ../neutron-dynamic-routing-common.templates:2001
msgid "Please enter the local BGP router id."
msgstr ""
